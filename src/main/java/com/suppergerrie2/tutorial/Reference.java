package com.suppergerrie2.tutorial;

public class Reference {
	public static final String MODID = "stm";
	public static final String MODNAME = "ClassicalBennurio";
	public static final String VERSION = "1.0";
	public static final String ACCEPTED_MINECRAFT_VERSIONS = "[1.12]";
}
