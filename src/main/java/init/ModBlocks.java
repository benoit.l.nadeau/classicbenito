package init;

import net.minecraft.block.Block;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import com.suppergerrie2.tutorial.Reference;
import blocks.BlockOreBenurite;
import items.ItemBasic;

@Mod.EventBusSubscriber(modid=Reference.MODID)
public class ModBlocks {

static Block benuriteBlock;
	
	public static void init() {
		benuriteBlock = new BlockOreBenurite("benurite",Material.ROCK, ModItems.benuriteOre,3,5).setCreativeTab(CreativeTabs.BUILDING_BLOCKS).setLightLevel(2.0f);
		benuriteBlock.setHarvestLevel("pickaxe", 3);
		
		
	}
	
	@SubscribeEvent
	public static void registerBlocks(RegistryEvent.Register<Block> event) {
		event.getRegistry().registerAll(benuriteBlock);
	}
	
	@SubscribeEvent
	public static void registerItemBlocks(RegistryEvent.Register<Item> event) {
		event.getRegistry().registerAll(new ItemBlock(benuriteBlock).setRegistryName(benuriteBlock.getRegistryName()));
	}
	
	@SubscribeEvent
	public static void registerRenders(ModelRegistryEvent event) {
		registerRender(Item.getItemFromBlock(benuriteBlock));
	}
	
	public static void registerRender(Item item) {
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation( item.getRegistryName(), "inventory"));
	}
}
