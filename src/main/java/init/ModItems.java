package init;

import com.suppergerrie2.tutorial.Reference;

import items.ItemBasic;
import tools.ItemCustomPickax;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent; 
import net.minecraftforge.common.util.EnumHelper;
@Mod.EventBusSubscriber(modid=Reference.MODID)
public class ModItems {
	
static Item benuriteIngot;
static Item benuriteOre;
static Item pickaxeBenurienne;

public static ToolMaterial MATERIAUX_BENURIEN = EnumHelper.addToolMaterial("bennurite", 3, 2500, 5F, 5F, 20);

	
	public static void init() {
		benuriteIngot = new ItemBasic("benurite_ingot");
		benuriteOre = new ItemBasic("benurite_ore");
		//pickaxeBenurienne = new ItemBasic("benurite_pickaxe");
		pickaxeBenurienne = new ItemCustomPickax("benurite_pickaxe",MATERIAUX_BENURIEN);
	}
	
	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> event) {
		event.getRegistry().registerAll(benuriteIngot);
		event.getRegistry().registerAll(benuriteOre);
		event.getRegistry().registerAll(pickaxeBenurienne);
	}
	
	@SubscribeEvent
	public static void registerRenders(ModelRegistryEvent event) {
		registerRender(benuriteIngot);
		registerRender(benuriteOre);
		registerRender(pickaxeBenurienne);
	}
	
	private static void registerRender(Item item) {
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation( item.getRegistryName(), "inventory"));
		
	}
	

}
