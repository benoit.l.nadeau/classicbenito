package tools;

import net.minecraft.item.ItemPickaxe;

public class ItemCustomPickax extends ItemPickaxe{

	public ItemCustomPickax(String name, ToolMaterial material) {
		super(material);
		this.setRegistryName(name);
		this.setUnlocalizedName(name);
	}

}
